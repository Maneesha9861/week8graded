package com.bean;

import org.springframework.stereotype.Component;

@Component
public class Books {
	private int bid;
	private String title;
	private String genre;
	private String imageurl;
	public int getBid() {
		return bid;
	}
	public void setBid(int bid) {
		this.bid = bid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public String getImageurl() {
		return imageurl;
	}
	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}
	@Override
	public String toString() {
		return "Books [bid=" + bid + ", title=" + title + ", genre=" + genre + ", imageurl=" + imageurl + "]";
	}

}

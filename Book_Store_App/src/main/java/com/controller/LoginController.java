package com.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.bean.Login;
import com.service.LoginService;

@Controller
public class LoginController {
	@Autowired
	LoginService loginService;
	@RequestMapping(value="welcome",method=RequestMethod.GET)
	public ModelAndView welcomepage() {
		ModelAndView mav=new ModelAndView();
		mav.setViewName("login.jsp");
		return mav;
	}
	@RequestMapping(value="Register",method=RequestMethod.GET)
	public ModelAndView Registrationpage() {
		ModelAndView mav=new ModelAndView();
		mav.setViewName("registration.jsp");
		return mav;
	}
	@RequestMapping(value="Registration",method=RequestMethod.POST)
	public ModelAndView StoreRegistrationDetails(HttpServletRequest req) {
		ModelAndView mav=new ModelAndView();
				
		String email=req.getParameter("email");
		String pass=req.getParameter("pass");
		
		Login log=new Login();
		log.setEmail(email);
		log.setPassword(pass);
	
		String result=loginService.storeRegistrationInfo(log);
		if(result.equalsIgnoreCase("sucess")) {
			req.setAttribute("logmsg","Your Registered Sucessfully");
			mav.setViewName("login.jsp");
			
		}else {
			req.setAttribute("regmsg","Your Registration is Failed! Please Try Again");
			mav.setViewName("index.jsp");	
		}			
		return mav;
	}
	@RequestMapping(value="login",method=RequestMethod.GET)
	public ModelAndView checkLoginDetails(HttpServletRequest req,HttpSession hs) {
		ModelAndView mav=new ModelAndView();
		String email=req.getParameter("email");
		String password=req.getParameter("pass");
		String user=email.substring(0, email.indexOf('@'));
		Login log=new Login();
		log.setEmail(email);
		log.setPassword(password);
	
		String res=loginService.checkLoginInfo(email);
		if(res.equalsIgnoreCase(password)) {
			hs.setAttribute("user",user);		
			mav.setViewName("dashboard.jsp");
		}else {
			req.setAttribute("flog","Sorry!Please Provide Details Correctly. ");
			mav.setViewName("index.jsp");
		}
		
		
		return mav;
		
	}
	

}


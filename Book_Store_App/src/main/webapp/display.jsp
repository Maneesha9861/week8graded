<%@page import="com.bean.Books"%>
<%@page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body style="background-color: azure;">
<h3 align="center"> List of Books Available </h3>
<a href="index.jsp">GO BACK</a>
<a href="welcome.spring">Login</a><br>
<div align="center">
<table border="1">
	<tr>
			<th>BId</th>
			<th>TITLE</th>
			<th>GENRE</th>
			<th>Image</th>
			
	</tr>
<% 
	Object obj = session.getAttribute("obj");
	List<Books> listOfBooks = (List<Books>)obj;
	Iterator<Books> ii = listOfBooks.iterator();
	while(ii.hasNext()){
		Books book  = ii.next();
		%>
		<tr>
			<td><%=book.getBid() %></td>
			<td><%=book.getTitle() %></td>
			<td><%=book.getGenre() %></td>
			<td><img src="<%=book.getImageurl()%>"height="200px" width="150px"></td>
			
		</tr>
		<% 
	}
%>
</table>
</div>
</body>
</html>
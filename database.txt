create database bookess_Maneesha;
use bookess_Maneesha;

create table Books(bid int primary key,title varchar(50), genre varchar(50),imageurl varchar(10000));

insert into Books values(1,"The MuffinMuncher","comic","https://i.pinimg.com/564x/4f/91/45/4f914557f2d361c380deb76b7e8756b2.jpg");

insert into books values(2,"HarryPotter","fictional","https://i.pinimg.com/564x/5c/78/f2/5c78f2041b5256174db6a752f4f29099.jpg");

insert into books values(3,"The Rose Code","novel","https://i.pinimg.com/564x/35/29/05/35290545e372d5419445507d0f5dbbf1.jpg");

 insert into books values(4,"Lost Apothecary","drama","https://i.pinimg.com/564x/c4/c4/78/c4c478b0eb5e454a0789e7cac575e27e.jpg");

insert into books values(5,"FireGlory","fictional","https://i.pinimg.com/564x/c4/65/57/c4655760adf9fe1843494490ab031806.jpg");

 insert into books values(6,"the Book Harlen","biography","https://i.pinimg.com/564x/ba/f8/45/baf8450f7e8bdfac4fcb95cb9ac70649.jpg");

 insert into books values(7,"Book of Last names ","drama","https://i.pinimg.com/564x/2b/d6/82/2bd682a02997994225ef43f94043d1e6.jpg");

insert into books values(8,"unfinished","thriller","https://images-eu.ssl-images-amazon.com/images/I/417FIfX3VTL._SY264_BO1,204,203,200_QL40_FMwebp_.jpg");


 select * from books;
 
 create table Login(email varchar(50) primary key,password varchar(50));
create table LikedBooks(bid int ,title varchar(50), genre varchar(50),imageurl varchar(10000));
create table  readlaterbooks(bid int ,title varchar(50), genre varchar(50),imageurl varchar(10000));
